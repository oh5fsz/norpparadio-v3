/**
 * Local environment settings
 *
 * Use this file to specify configuration settings for use while developing
 * the app on your personal system.
 *
 * For more information, check out:
 * https://sailsjs.com/docs/concepts/configuration/the-local-js-file
 */

module.exports = {

  // Any configuration settings may be overridden below, whether it's built-in Sails
  // options or custom configuration specifically for your app (e.g. Stripe, Mailgun, etc.)
  enableShoutbox: process.env.ENABLE_SHOUTBOX || false,
  enablePlayer: process.env.ENABLE_PLAYER || false,
  enableProgramCalendar: process.env.ENABLE_PROGRAM_CALENDAR || false
  
};
