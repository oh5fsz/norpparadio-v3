/**
 * Shout.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    nick: {
      type: 'string',
      required: true,
      minLength: 2,
      maxLength: 32
    },
    message: {
      type: 'string',
      required: true,
      minLength: 1,
      maxLength: 255
    },
  }

};

