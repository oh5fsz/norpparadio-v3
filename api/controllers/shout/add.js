/**
 * Module dependencies
 */

// ...


/**
 * shout/add.js
 *
 * Add shout.
 */
module.exports = async function add(req, res) {

  if (!req.isSocket) {
    return res.badRequest('Socket clients only');
  }

  var shout = await Shout.create({'nick': req.body['nickname'], 'message': req.body['message']}).fetch();
  console.log(shout);
  sails.sockets.broadcast('shoutSockets', shout);
  return res.ok();

};
