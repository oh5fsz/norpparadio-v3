/**
 * Module dependencies
 */

// ...


/**
 * shout/join.js
 *
 * Join shout.
 */
module.exports = async function join(req, res) {

  if (!req.isSocket) {
    return res.badRequest('Socket clients only');
  }
  sails.sockets.join(req, 'shoutSockets');
  sails.log('New shoutbox client!');
  return res.ok();
};
