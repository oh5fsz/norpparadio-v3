/**
 * Module dependencies
 */

// ...


/**
 * shout/latest.js
 *
 * Latest shout.
 */
module.exports = async function latest(req, res) {

  var shouts = await Shout.find().sort([{createdAt: 'DESC'}]).limit(20);
  return res.json(shouts.reverse());
};
