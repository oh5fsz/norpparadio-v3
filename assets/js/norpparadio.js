var g_csrfToken;
var g_viewModel;


function Norpparadio () {
  g_csrfToken = window.SAILS_LOCALS._csrf;
  io.socket.headers = { 'x-csrf-token': g_csrfToken }
  this.shouts = ko.observableArray([]);
  this.playing = ko.observable(false);

  this.nickToSend = ko.observable();
  this.messageToSend = ko.observable();

  console.log('CSRF Token: ', g_csrfToken);

  this.getPlayButtonClasses = ko.pureComputed(function () {
    if (this.playing()) {
      return "fa-stop-circle";
    }
    else {
      return "fa-play-circle";
    }
  }, this);
}

Norpparadio.prototype.playButton = function () {
  console.log(this.playing());
  if (!this.playing()) {
    console.log('Play!')
    $('#jquery_jplayer_1').jPlayer("play")
    this.playing(true);
  }
  else if (this.playing()) {
    console.log("Pausing")
    $('#jquery_jplayer_1').jPlayer("pause")
    this.playing(false);
  }
}

Norpparadio.prototype.disableSendButton = function () {
  $("#sendbutton").prop('disabled', true);
}

Norpparadio.prototype.enableSendButton = function () {
  $("#sendbutton").prop('disabled', false);
}

Norpparadio.prototype.sendShout = function () {
  this.disableSendButton();
  setTimeout(this.enableSendButton, 1000);
  console.log('Sendshout');
  var data_to_send = {
    'nickname': this.nickToSend(),
    'message': this.messageToSend(),
  }
  io.socket.post('/shoutbox/add', data_to_send, function (resData, jwRes) {
    console.log(resData, jwRes);
  });
  this.messageToSend('');
}

Norpparadio.prototype.addShout = function (shout) {
  this.shouts.push(shout);
}

if (window.SAILS_LOCALS.enableShoutbox) {
  io.socket.get('/shoutbox/join', function res (data, jwRes) {
    console.log('Server responded with status code ' + jwRes.statusCode + ' data: ', data)
  })

  io.socket.on('message', function (data) {
    data.timestamp = moment(data.createdAt).format('HH:mm:ss');
    g_viewModel.shouts.push(data);
    $("#shoutsul").scrollTop($("#shoutsul")[0].scrollHeight);
  })
}



$(document).ready(function () {
  g_viewModel = new Norpparadio();
  ko.applyBindings(g_viewModel);
  console.log('Bindings applied.');

  if (window.SAILS_LOCALS.enablePlayer) {
    console.log('Enabling player');
    $('#jquery_jplayer_1').jPlayer({
      ready: function () {
        $(this).jPlayer('setMedia', {
          title: 'Norpparadio',
          mp3: 'http://norpparadio.lnet.fi:8000/norpparadio.mp3',
          oga: 'http://norpparadio.lnet.fi:8000/norpparadio.ogg'
        })
      },
      swfPath: '/js',
      supplied: 'mp3, oga'
    })
  }

  if (window.SAILS_LOCALS.enableShoutbox) {
    io.socket.get('/shoutbox/latest', function res (data, jwRes) {
      $.each(data, function (i, data) {
        data.timestamp = moment(data.createdAt).format('HH:mm:ss');
        g_viewModel.shouts.push(data);
      })
      console.log(g_viewModel.shouts());
      $("#shoutsul").scrollTop($("#shoutsul")[0].scrollHeight);
    })
  }

  if (window.SAILS_LOCALS.enableProgramCalendar) {
    $('#calendar').fullCalendar({
      googleCalendarApiKey: 'AIzaSyCC4AGNXC0gZ7jqwWDxbFWCePJONHNG_Qs',
      events: {
        googleCalendarId: 'norpparadio.net_ssfjrv63euk1pt9qvgtg7b90mg@group.calendar.google.com'
      },
      defaultView: 'agendaWeek',
      timeFormat: 'H(:mm)',
      minTime: '12:00:00',
      maxTime: '20:00:00',
      contentHeight: 'auto',
      allDaySlot: false,
      firstDay: 1,
    });
  }

  $('#message').keyup(function (event) {
    if (event.keyCode === 13) {
      console.log('Enter!');
      g_viewModel.sendShout();
    }
  });

})
